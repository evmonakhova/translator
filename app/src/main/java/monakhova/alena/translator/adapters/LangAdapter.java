package monakhova.alena.translator.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import monakhova.alena.translator.mvp.model.Language;

/**
 * Created by alena on 15.08.2016.
 */
public class LangAdapter extends ArrayAdapter<Language> {
    private Context context;
    private CheckedTextView textView;

    public LangAdapter(Context context, int resource, ArrayList<Language> objects) {
        super(context, resource, objects);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        LayoutInflater inflater = (LayoutInflater)context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (view == null)
            view = inflater.inflate(android.R.layout.simple_list_item_single_choice, parent, false);

        Language lang = getItem(position);
        textView = ((CheckedTextView)view.findViewById(android.R.id.text1));
        textView.setText(lang.getName());

        return view;
    }
}
