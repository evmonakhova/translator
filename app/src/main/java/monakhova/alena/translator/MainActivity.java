package monakhova.alena.translator;

import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.widget.DrawerLayout;

import monakhova.alena.translator.fragments.FavoritesFragment;
import monakhova.alena.translator.fragments.NavigationDrawerFragment;
import monakhova.alena.translator.fragments.SettingsFragment;
import monakhova.alena.translator.fragments.TranslationFragment;

import static monakhova.alena.translator.mvp.interactor.ILangsInteractor.*;


public class MainActivity extends AppCompatActivity implements
        NavigationDrawerFragment.NavigationDrawerCallbacks {

    private static String TAG = MainActivity.class.getSimpleName();

    private String[] mScreenTitles;
    private int currentPosition;

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private FragmentManager fragmentManager;
    private FragmentHelper fragmentHelper;

    private OnLangChangedListener onLangChangedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));
        setSupportActionBar(toolbar);

        ColorDrawable color = new ColorDrawable();
        color.setColor(ContextCompat.getColor(this, R.color.orange));
        getSupportActionBar().setBackgroundDrawable(color);

        mScreenTitles = getResources().getStringArray(R.array.menu_items);

        fragmentManager = getSupportFragmentManager();

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                fragmentManager.findFragmentById(R.id.navigation_drawer);
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
                (DrawerLayout)findViewById(R.id.drawer_layout));
    }

    @Override
    public void onBackPressed() {
        fragmentHelper.popBackStack();
        restoreActionBar();
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        if (fragmentManager == null)
            return;

        fragmentHelper = new FragmentHelper(fragmentManager);
        String tag = "fragment_" + position;
        Fragment fragment = fragmentHelper.getFragmentByTag(tag);

//        Log.d(TAG, "tag: " + tag + ", fragment: " + fragment + ", fm:" + fragmentManager);

        currentPosition = position;
        restoreActionBar();

        if (fragment == null) {
            switch (position) {
                case 0:
                    fragment = TranslationFragment.newInstance(1);
                    break;
                case 1:
                    fragment = FavoritesFragment.newInstance(2);
                    break;
                case 2:
                    fragment = SettingsFragment.newInstance(3);
                    break;
            }
            fragmentHelper.addFragment(fragment, tag);
        } else {
            fragmentHelper.replaceFragment(fragment, tag);
        }
    }

    public void setupToolbarWithTitle(String title) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null)
            return;
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(false);
        actionBar.setTitle(title);
    }

    private void setupToolbarWithCustomView(int id) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null)
            return;
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
        actionBar.setCustomView(getLayoutInflater().inflate(id, null), params);
    }

    private void restoreActionBar() {
        if (currentPosition == 0) {
            setupToolbarWithCustomView(R.layout.toolbar_langs_layout);
            TranslationFragment fragment = (TranslationFragment) fragmentHelper.getFragmentByTag("fragment_0");
            if (fragment != null)
                fragment.initToolbar();
        } else {
            setupToolbarWithTitle(mScreenTitles[currentPosition]);
        }
    }


    public void setOnLangChangedListener(OnLangChangedListener listener){
        this.onLangChangedListener = listener;
    }

    public OnLangChangedListener getOnLangChangedListener(){
        return onLangChangedListener;
    }

}
