package monakhova.alena.translator.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import monakhova.alena.translator.adapters.LangAdapter;
import monakhova.alena.translator.MainActivity;
import monakhova.alena.translator.R;
import monakhova.alena.translator.mvp.model.Language;
import monakhova.alena.translator.mvp.presenter.ILanguagesPresenter;
import monakhova.alena.translator.mvp.presenter.LanguagesPresenter;
import monakhova.alena.translator.mvp.view.ILanguagesView;
import monakhova.alena.translator.utils.Utils;

/**
 * Created by alena on 14.08.2016.
 */
public class LanguagesFragment extends ListFragment implements ILanguagesView {

    private static String TAG = LanguagesFragment.class.getSimpleName();

    private static final String FR_TYPE = "type";

    private Activity activity;
    private ListView listView;
    private LangAdapter adapter;

    private ILanguagesPresenter langsPresenter;

    public static LanguagesFragment newLangFragmentInstance(int fragmentType) {
        LanguagesFragment fragment = new LanguagesFragment();
        Bundle args = new Bundle();
        args.putInt(FR_TYPE, fragmentType);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = getActivity();
        this.langsPresenter = new LanguagesPresenter(activity, getArguments().getInt(FR_TYPE));
        langsPresenter.attachView(this);
    }

    @Override
    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        langsPresenter.onCreate();
        View rootView = inflater.inflate(R.layout.fragment_languages, container, false);
        showGlobalContextActionBar();
        return rootView;
    }

    @Override
    public void onStart(){
        super.onStart();
        listView = getListView();
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        langsPresenter.onStart();
    }

    @Override
    public void showToast(String msg) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showItems(ArrayList<Language> langs) {
        adapter = new LangAdapter(activity,
                android.R.layout.simple_list_item_single_choice, langs){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView textView = (TextView) super.getView(position, convertView, parent);
                textView.setTextColor(ContextCompat.getColor(getContext(), R.color.dark_grey));
                return textView;
            }
        };
        setListAdapter(adapter);
    }

    @Override
    public void setLangChecked(Language currentLang) {
        listView.setItemChecked(currentLang.getId(), true);
    }

    @Override
    public void onListItemClick(ListView listView, View view, int pos, long id){
        super.onListItemClick(listView, view, pos, id);
        listView.setItemChecked(pos, true);
        langsPresenter.onListItemClick(listView, view, pos, id);
        adapter.notifyDataSetChanged();
        activity.onBackPressed();
    }

    private void showGlobalContextActionBar() {
        int fragmentType = getArguments().getInt(FR_TYPE);
        String title;
        if (fragmentType == Utils.SRC_LANG)
            title = getString(R.string.source_language_text);
        else
            title = getString(R.string.target_language_text);

        ((MainActivity)activity).setupToolbarWithTitle(title);
    }
}
