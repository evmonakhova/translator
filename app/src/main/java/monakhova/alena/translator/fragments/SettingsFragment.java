package monakhova.alena.translator.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import monakhova.alena.translator.MainActivity;
import monakhova.alena.translator.R;

/**
 * Created by alena on 12.08.2016.
 */
public class SettingsFragment extends Fragment {

    private static String TAG = SettingsFragment.class.getName();

    private static final String ARG_SECTION_NUMBER = "section_number";

    private Activity activity;
    private View rootView;

    public static SettingsFragment newInstance(int sectionNumber) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach()");
        this.activity = getActivity();
    }

    @Override
    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView()");
        rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        return rootView;
    }

}
