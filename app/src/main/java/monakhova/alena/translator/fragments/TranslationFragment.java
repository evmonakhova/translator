package monakhova.alena.translator.fragments;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import monakhova.alena.translator.FragmentHelper;
import monakhova.alena.translator.MainActivity;
import monakhova.alena.translator.R;
import monakhova.alena.translator.mvp.model.Language;
import monakhova.alena.translator.mvp.presenter.ITranslationPresenter;
import monakhova.alena.translator.mvp.presenter.TranslationPresenter;
import monakhova.alena.translator.mvp.view.ITranslationView;
import monakhova.alena.translator.utils.Utils;

/**
 * Created by alena on 12.08.2016.
 */
public class TranslationFragment extends Fragment implements ITranslationView,
        View.OnClickListener {

    private static String TAG = TranslationFragment.class.getSimpleName();

    private static final String ARG_SECTION_NUMBER = "section_number";

    private Activity activity;
    private View rootView;

    private Button srcLangButton;
    private Button targetLangButton;
    private String srcLang = null;
    private String targetLang = null;

    @BindView(R.id.favorite_button)
    protected ImageButton favoriteButton;
    @BindView(R.id.input_text)
    protected EditText inputText;
    @BindView(R.id.translation_text)
    protected TextView translationText;

    private ITranslationPresenter translationPresenter;

    private boolean favorite = false;

    public static TranslationFragment newInstance(int sectionNumber) {
        TranslationFragment fragment = new TranslationFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = getActivity();
        this.translationPresenter = new TranslationPresenter(activity);
        translationPresenter.attachView(this);
    }

    @Override
    public void onCreate(Bundle savedState) {
        super.onCreate(savedState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle state) {
        translationPresenter.onCreate();
        rootView = inflater.inflate(R.layout.fragment_translation, container, false);
        rootView.setOnClickListener(this);

        ButterKnife.bind(this, rootView);

        inputText.setHorizontallyScrolling(false);
        if (state != null)
            translationText.setText(state.getCharSequence("translation"));

        initToolbar();

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle state){
        super.onSaveInstanceState(state);
        state.putCharSequence("translation", translationText.getText());
    }

    @Override
    public void onStart(){
        super.onStart();
        translationPresenter.onStart();
    }

    @Override
    @OnClick({ R.id.clean_button, R.id.ok_button, R.id.favorite_button, R.id.copy_button })
    public void onClick(View view) {
        translationPresenter.onClick(view);
    }

    @Override
    public void startLangFragment(int fragmentType) {
        String tag;
        if (fragmentType == Utils.SRC_LANG)
            tag = "fragment_src";
        else
            tag = "fragment_target";

        FragmentManager fm = ((MainActivity)activity).getSupportFragmentManager();
        FragmentHelper fragmentHelper = new FragmentHelper(fm);

        Fragment fragment = fragmentHelper.getFragmentByTag(tag);
        if (fragment == null)
            fragment = LanguagesFragment.newLangFragmentInstance(fragmentType);

        fragmentHelper.addFragmentToBackStack(fragment, tag);
    }

    @Override
    public void setLang(String lang, int langType) {
        if (langType == Utils.SRC_LANG) {
            srcLang = lang;
            srcLangButton.setText(lang);
        } else if (langType == Utils.TARGET_LANG) {
            targetLang = lang;
            targetLangButton.setText(lang);
        }
    }

    @Override
    public void setLang(Language lang, int langType) {
        setLang(lang.getName(), langType);
    }

    @Override
    public void showTranslation(String translation) {
        translationText.setText(translation);
    }

    @Override
    public void cleanTextField() {
        inputText.setText("");
        translationText.setText("");
    }

    @Override
    public void setFavoriteButtonStatus(boolean isFavorite) {
        favorite = !favorite;
        favoriteButton.setSelected(favorite);
    }

    @Override
    public void copyText() {
        translationText.setSelected(true);
        CharSequence text = translationText.getText();

        ClipData clipData = ClipData.newPlainText("Translation", text);
        ClipboardManager clipboard = (ClipboardManager) activity
                .getSystemService(Activity.CLIPBOARD_SERVICE);
        clipboard.setPrimaryClip(clipData);

        Toast.makeText(activity, getString(R.string.text_copied), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showToast(String msg) {
        Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
    }

    public void initToolbar() {
        ActionBar actionBar = ((AppCompatActivity)activity).getSupportActionBar();
        View customView = actionBar.getCustomView();
        srcLangButton = (Button) customView.findViewById(R.id.src_lang_button);
        targetLangButton = (Button) customView.findViewById(R.id.target_lang_button);
        ImageButton swapButton = (ImageButton) customView.findViewById(R.id.swap_button);
        srcLangButton.setOnClickListener(this);
        targetLangButton.setOnClickListener(this);
        swapButton.setOnClickListener(this);
        if (srcLang != null && targetLang != null){
            srcLangButton.setText(srcLang);
            targetLangButton.setText(targetLang);
        }
    }
}
