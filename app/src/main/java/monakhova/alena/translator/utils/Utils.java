package monakhova.alena.translator.utils;

import java.util.Locale;

/**
 * Created by alena on 14.08.2016.
 */
public class Utils {

    private static String TAG = Utils.class.getName();

    public static final int SRC_LANG = 1;
    public static final int TARGET_LANG = 2;

    public static String getCurrentLangCode() {
        return Locale.getDefault().getLanguage();
    }

    public static String getCurrentLang() {
        String lang = Locale.getDefault().getDisplayLanguage();
        lang = lang.substring(0, 1).toUpperCase() + lang.substring(1);
        return lang;
    }
}
