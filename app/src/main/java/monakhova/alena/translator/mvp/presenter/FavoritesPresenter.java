package monakhova.alena.translator.mvp.presenter;

import android.view.View;

import monakhova.alena.translator.mvp.view.IFavoritesView;

/**
 * Created by alena on 13.08.2016.
 */
public class FavoritesPresenter implements IPresenter<IFavoritesView> {
    @Override
    public void onCreate() {

    }

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void attachView(IFavoritesView view) {

    }
}
