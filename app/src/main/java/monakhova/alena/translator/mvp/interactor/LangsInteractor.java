package monakhova.alena.translator.mvp.interactor;

import android.util.Log;

import java.util.ArrayList;

import monakhova.alena.translator.mvp.model.Language;
import monakhova.alena.translator.mvp.model.Languages;
import monakhova.alena.translator.net.ApiClient;
import monakhova.alena.translator.net.IServerAPI;
import monakhova.alena.translator.storage.LanguagesStorage;
import monakhova.alena.translator.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by alena on 15.08.2016.
 */
public class LangsInteractor implements ILangsInteractor {

    private static String TAG = LangsInteractor.class.getSimpleName();

    @Override
    public void getLangsRequest(String ui, final OnLangsReceivedListener onLangsReceivedListener) {

        IServerAPI apiService = ApiClient.getClient().create(IServerAPI.class);
        Call<Languages> call = apiService.getLangs(ApiClient.KEY, ui);

        Log.i(TAG, "langs request:" + call.request());

        call.enqueue(new Callback<Languages>() {
            @Override
            public void onResponse(Call<Languages>call, Response<Languages> response) {
                Languages langs = response.body();
                if (langs != null) {
                    Language src = getDefaultLang(langs.getLangs(), Utils.SRC_LANG);
                    Language target = getDefaultLang(langs.getLangs(), Utils.TARGET_LANG);
                    onLangsReceivedListener.onSuccess(langs, src, target);
                } else {
                    Log.e("TAG", "Null response!!!");
                }
            }
            @Override
            public void onFailure(Call<Languages>call, Throwable t) {
                Log.e(TAG, t.toString());
                onLangsReceivedListener.onServerError(t.toString());
            }
        });
    }

    @Override
    public void setCurrentLang(Language lang, int langType, LanguagesStorage langsStorage,
                               OnLangChangedListener onLangChangedListener) {

        langsStorage.setCurrentLang(lang, langType);
        onLangChangedListener.onLangChanged(lang, langType);
    }

    @Override
    public Language getDefaultLang(ArrayList<Language> languages, int langType){
        String code = "";
        Language lang = null;

        if (langType == Utils.SRC_LANG)
            code = Utils.getCurrentLangCode();
        else if (langType == Utils.TARGET_LANG)
            code = "en";

        for (int i = 0; i < languages.size(); i++){
            Language item = languages.get(i);
            if (item.getCode().equals(code)) {
                lang = item;
                break;
            }
        }

        return lang;
    }
}
