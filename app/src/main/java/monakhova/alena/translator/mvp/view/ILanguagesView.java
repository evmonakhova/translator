package monakhova.alena.translator.mvp.view;

import java.util.ArrayList;

import monakhova.alena.translator.mvp.model.Language;

/**
 * Created by alena on 14.08.2016.
 */
public interface ILanguagesView extends IView {

    void showItems(ArrayList<Language> langs);

    void setLangChecked(Language currentLang);

}
