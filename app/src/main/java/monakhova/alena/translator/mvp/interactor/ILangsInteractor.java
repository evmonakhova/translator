package monakhova.alena.translator.mvp.interactor;

import java.util.ArrayList;

import monakhova.alena.translator.mvp.model.Language;
import monakhova.alena.translator.mvp.model.Languages;
import monakhova.alena.translator.storage.LanguagesStorage;

/**
 * Created by alena on 15.08.2016.
 */
public interface ILangsInteractor {

    interface OnLangsReceivedListener {
        void onServerError(String msg);
        void onSuccess(Languages langs, Language srcLang, Language targetLang);
    }

    interface OnLangChangedListener {
        void onLangChanged(Language lang, int langCode);
    }

    public void getLangsRequest(String ui, final OnLangsReceivedListener onLangsReceivedListener);

    public void setCurrentLang(Language lang, int langType, LanguagesStorage langsStorage,
                               OnLangChangedListener onLangChangedListener);

    public Language getDefaultLang(ArrayList<Language> languages, int langType);

}
