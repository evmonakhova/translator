package monakhova.alena.translator.mvp.model;

/**
 * Created by alena on 13.08.2016.
 */
public class Engine {

    private int id;
    private String name;

    public Engine(String name){
        this.name = name;
    }
    
    public String getName(){
        return name;
    }

}
