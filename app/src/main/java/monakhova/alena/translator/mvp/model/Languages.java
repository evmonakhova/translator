package monakhova.alena.translator.mvp.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by alena on 13.08.2016.
 */
public class Languages {

    @SerializedName("langs")
    private TreeMap<String, String> langsMap = new TreeMap<>();

    public Languages(TreeMap<String, String> langsMap){
        this.langsMap = langsMap;
    }

    public ArrayList<Language> getLangs(){
        List<Map.Entry<String, String>> list = new LinkedList<>(langsMap.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, String>>() {
            @Override
            public int compare(Map.Entry<String, String> e1, Map.Entry<String, String> e2) {
                return e1.getValue().compareTo(e2.getValue());
            }
        });

        ArrayList<Language> langs = new ArrayList<>();
        int i = 0;
        for (Map.Entry<String, String> entry : list) {
            langs.add(new Language(i, entry.getKey(), entry.getValue()));
            i++;
        }

        return langs;
    }
}
