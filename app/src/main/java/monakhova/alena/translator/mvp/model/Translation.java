package monakhova.alena.translator.mvp.model;

import com.google.gson.annotations.SerializedName;


/**
 * Created by alena on 12.08.2016.
 */
public class Translation {

    @SerializedName("code")
    private Integer code;

    @SerializedName("lang")
    private String lang;

    @SerializedName("text")
    private String[] translationText = new String[10000];

    private long id;
    private String sourceText;
    private String sourceLang;
    private String targetLang;
    private boolean isFavorite;

    public Translation(Integer code, String[] translationText, String lang){
        this.code = code;
        this.lang = lang;
        this.translationText = translationText;
        setSourceTargetLangs(lang);
    }

    public long getCode(){
        return code;
    }

    public void setCode(Integer code){
        this.code = code;
    }

    public String getLang(){
        return lang;
    }

    public void setLang(String lang){
        this.lang = lang;
    }

    public String[] getTranslationText(){
        return translationText;
    }

    public void setTranslationText(String[] translationText){
        this.translationText = translationText;
    }


    public long getId(){
        return id;
    }

    public void setId(long id){
       this.id = id;
    }

    public String getSourceText(){
        return sourceText;
    }

    public void setSourceText(String text){
        this.sourceText = text;
    }

    public String getSourceLang(){
        return sourceLang;
    }

    public String getTargetLang(){
        return targetLang;
    }

    public void setSourceTargetLangs(String lang){
        if (!lang.equals("")) {
            this.sourceLang = lang.split("\\.")[0];
            this.targetLang = lang.split("\\.")[1];
        }
    }

    public boolean isFavorite(){
        return this.isFavorite;
    }

    public void setFavorite(boolean isFavorite){
        this.isFavorite = isFavorite;
    }

}
