package monakhova.alena.translator.mvp.view;


/**
 * Created by alena on 12.08.2016.
 */
public interface IView {

    void showToast(String msg);

}
