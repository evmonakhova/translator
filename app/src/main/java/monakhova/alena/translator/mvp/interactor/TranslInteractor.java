package monakhova.alena.translator.mvp.interactor;

import android.util.Log;

import monakhova.alena.translator.mvp.model.Languages;
import monakhova.alena.translator.mvp.model.Translation;
import monakhova.alena.translator.net.ApiClient;
import monakhova.alena.translator.net.IServerAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by alena on 12.08.2016.
 */
public class TranslInteractor implements ITranslInteractor {

    private static String TAG = TranslInteractor.class.getSimpleName();

    @Override
    public void translationRequest(String srcText, String lang,
                           final OnTranslReceivedListener onTranslReceivedListener) {

        IServerAPI apiService = ApiClient.getClient().create(IServerAPI.class);
        Call<Translation> call = apiService.translate(ApiClient.KEY, srcText, lang);

        Log.i(TAG, "translation request:" + call.request());

        call.enqueue(new Callback<Translation>() {
            @Override
            public void onResponse(Call<Translation> call, Response<Translation> response) {
                Translation translation = response.body();
                if (translation != null) {
                    Log.i(TAG, "Response: " + translation.getTranslationText());
                    onTranslReceivedListener.onSuccess(translation);
                } else {
                    Log.e("TAG", "Null response!!!");
                }
            }

            @Override
            public void onFailure(Call<Translation> call, Throwable t) {
                Log.e(TAG, t.toString());
                onTranslReceivedListener.onServerError(t.toString());
            }
        });
    }
}
