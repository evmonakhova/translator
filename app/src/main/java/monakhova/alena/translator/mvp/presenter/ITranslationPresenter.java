package monakhova.alena.translator.mvp.presenter;

import android.view.View;

import monakhova.alena.translator.mvp.view.ITranslationView;

/**
 * Created by alena on 15.08.2016.
 */
public interface ITranslationPresenter extends IPresenter<ITranslationView> {

    void onClick(View view);

}
