package monakhova.alena.translator.mvp.view;

import monakhova.alena.translator.mvp.model.Language;

/**
 * Created by alena on 12.08.2016.
 */
public interface ITranslationView extends IView {

    void startLangFragment(int fragmentType);

    void setLang(String srcLang, int langType);

    void setLang(Language srcLang, int langType);

    void showTranslation(String translation);

    void cleanTextField();

    void setFavoriteButtonStatus(boolean isFavorite);

    void copyText();

    void showToast(String msg);

    void hideKeyboard();

}
