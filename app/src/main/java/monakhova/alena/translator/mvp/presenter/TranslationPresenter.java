package monakhova.alena.translator.mvp.presenter;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

import monakhova.alena.translator.MainActivity;
import monakhova.alena.translator.R;
import monakhova.alena.translator.mvp.interactor.ILangsInteractor;
import monakhova.alena.translator.mvp.interactor.ITranslInteractor;
import monakhova.alena.translator.mvp.interactor.LangsInteractor;
import monakhova.alena.translator.mvp.interactor.TranslInteractor;
import monakhova.alena.translator.mvp.model.Language;
import monakhova.alena.translator.mvp.model.Languages;
import monakhova.alena.translator.mvp.model.Translation;
import monakhova.alena.translator.mvp.view.ITranslationView;
import monakhova.alena.translator.storage.LanguagesStorage;
import monakhova.alena.translator.utils.Utils;

import static monakhova.alena.translator.mvp.interactor.ILangsInteractor.*;
import static monakhova.alena.translator.mvp.interactor.ILangsInteractor.OnLangChangedListener;
import static monakhova.alena.translator.mvp.interactor.ITranslInteractor.*;

/**
 * Created by alena on 12.08.2016.
 */
public class TranslationPresenter implements ITranslationPresenter {

    private static String TAG = TranslationPresenter.class.getSimpleName();

    private Activity activity;
    private ITranslationView translationView;
    private ITranslInteractor translationInteractor;
    private ILangsInteractor langsInteractor;
    private LanguagesStorage langsStorage;
    private Language srcLanguage;
    private Language targetLanguage;
    private ArrayList<Language> languages;

    public TranslationPresenter(Activity activity){
        this.activity = activity;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate()");
        this.translationInteractor = new TranslInteractor();
        this.langsInteractor = new LangsInteractor();
        this.langsStorage = LanguagesStorage.getInstance(activity);
    }

    @Override
    public void attachView(ITranslationView view) {
        this.translationView = view;
    }

    @Override
    public void onStart() {
        ((MainActivity)activity).setOnLangChangedListener(onLangChangedListener);
        initLangs();
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.src_lang_button:
                langsStorage.setCurrentLang(srcLanguage, Utils.SRC_LANG);
                translationView.startLangFragment(Utils.SRC_LANG);
                break;
            case R.id.target_lang_button:
                langsStorage.setCurrentLang(targetLanguage, Utils.TARGET_LANG);
                translationView.startLangFragment(Utils.TARGET_LANG);
                break;
            case R.id.swap_button:
                swapLanguages();
                break;
            case R.id.clean_button:
                translationView.cleanTextField();
                break;
            case R.id.ok_button:
                translationView.hideKeyboard();
                EditText inputText = (EditText)activity.findViewById(R.id.input_text);
                String srcText = inputText.getText().toString();
                String trDir = getTranslateDirection(srcLanguage, targetLanguage);
                translationInteractor.translationRequest(srcText, trDir, onTranslReceivedListener);
                break;
            case R.id.favorite_button:
                translationView.setFavoriteButtonStatus(true);
                break;
            case R.id.copy_button:
                translationView.copyText();
                break;
        }
    }

    private void initLangs() {
        srcLanguage = langsStorage.getCurrentLang(Utils.SRC_LANG);
        targetLanguage = langsStorage.getCurrentLang(Utils.TARGET_LANG);

        if (srcLanguage != null && targetLanguage != null) {
            translationView.setLang(srcLanguage, Utils.SRC_LANG);
            translationView.setLang(targetLanguage, Utils.TARGET_LANG);
        } else {
            String currentLang = Utils.getCurrentLangCode();
            Log.d(TAG, "currentLang: " + currentLang);
            langsInteractor.getLangsRequest(currentLang,
                    onLangsReceivedListener);
        }
    }

    private void setLanguage(Language lang, int langType){
        if (langType == Utils.SRC_LANG)
            srcLanguage = lang;
        else
            targetLanguage = lang;

        langsStorage.setCurrentLang(lang, langType);
        translationView.setLang(lang, langType);
    }

    private void swapLanguages(){
        Language temp = targetLanguage;
        targetLanguage = srcLanguage;
        srcLanguage = temp;
        setLanguage(srcLanguage, Utils.SRC_LANG);
        setLanguage(targetLanguage, Utils.TARGET_LANG);
    }

    private String getTranslateDirection(Language srcLang, Language targetLang){
        String dir = "ru-en";

        if (srcLang == null || targetLang == null){
            srcLang = langsStorage.getCurrentLang(Utils.SRC_LANG);
            targetLang = langsStorage.getCurrentLang(Utils.TARGET_LANG);
        }

        String srcCode = srcLang.getCode();
        String targetCode = targetLang.getCode();
        Log.d(TAG, "src: " + srcCode + ", target: " + targetCode);

        if (!srcCode.equals("") && !targetCode.equals("")){
            dir = srcCode + "-" + targetCode;
        } else if (targetLang.getName().equals(activity.getString(R.string.target_lang))) {
            dir = "en-ru";
        }
        Log.d(TAG, "translate dir: " + dir);
        return dir;
    }

    OnLangsReceivedListener onLangsReceivedListener = new OnLangsReceivedListener() {
        @Override
        public void onServerError(String msg) {
            Log.e(TAG, "Server error: " + msg);
            translationView.showToast(activity.getString(R.string.on_server_error));
        }
        @Override
        public void onSuccess(Languages response, Language srcLang, Language targetLang) {
            if (response.getLangs() != null) {
                Log.d(TAG, "onSuccess. languages size: " + response.getLangs().size());
                languages = response.getLangs();
                langsStorage.setLanguages(languages);
                srcLanguage = srcLang;
                targetLanguage = targetLang;
                setLanguage(srcLanguage, Utils.SRC_LANG);
                setLanguage(targetLanguage, Utils.TARGET_LANG);
            } else {
                Log.e(TAG, "Languages response is null!!!");
            }
        }
    };

    OnTranslReceivedListener onTranslReceivedListener = new OnTranslReceivedListener() {
        @Override
        public void onServerError(String msg) {
            Log.e(TAG, "Server error: " + msg);
            translationView.showToast(activity.getString(R.string.on_server_error));
        }
        @Override
        public void onSuccess(Translation response) {
            String[] text = response.getTranslationText();
            if (text != null && text.length > 0) {
                translationView.showTranslation(response.getTranslationText()[0]);
            } else {
                Log.e(TAG, "Translation response is null!!!");
            }
        }
    };

    OnLangChangedListener onLangChangedListener = new OnLangChangedListener() {
        @Override
        public void onLangChanged(Language lang, int langType) {
            setLanguage(lang, langType);
        }
    };
}
