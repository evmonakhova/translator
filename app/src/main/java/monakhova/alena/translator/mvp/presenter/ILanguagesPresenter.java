package monakhova.alena.translator.mvp.presenter;

import android.view.View;
import android.widget.ListView;

import monakhova.alena.translator.mvp.view.ILanguagesView;
import monakhova.alena.translator.mvp.view.ITranslationView;

/**
 * Created by alena on 15.08.2016.
 */
public interface ILanguagesPresenter extends IPresenter<ILanguagesView> {

    void onListItemClick(ListView listView, View view, int pos, long id);

}
