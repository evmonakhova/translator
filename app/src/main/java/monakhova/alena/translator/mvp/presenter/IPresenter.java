package monakhova.alena.translator.mvp.presenter;

import android.os.Bundle;

import monakhova.alena.translator.mvp.view.IView;

/**
 * Created by alena on 12.08.2016.
 */
public interface IPresenter<T extends IView> {

    void onCreate();

    void onStart();

//    void onSaveInstanceState(Bundle state);

    void onStop();

    void onDestroy();

    void attachView(T view);

}
