package monakhova.alena.translator.mvp.model;

/**
 * Created by alena on 15.08.2016.
 */
public class Language {

    private int id;
    private String code;
    private String name;

    public Language(int id, String code, String name){
        this.id = id;
        this.code = code;
        this.name = name;
    }

    public Language(String code){
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public String getCode(){
        return code;
    }

    public String getName(){
        return name;
    }

}
