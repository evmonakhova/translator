package monakhova.alena.translator.mvp.presenter;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

import monakhova.alena.translator.MainActivity;
import monakhova.alena.translator.R;
import monakhova.alena.translator.mvp.interactor.ILangsInteractor;
import monakhova.alena.translator.mvp.interactor.LangsInteractor;
import monakhova.alena.translator.mvp.model.Language;
import monakhova.alena.translator.mvp.model.Languages;
import monakhova.alena.translator.mvp.view.ILanguagesView;
import monakhova.alena.translator.storage.LanguagesStorage;
import monakhova.alena.translator.utils.Utils;

import static monakhova.alena.translator.mvp.interactor.ILangsInteractor.*;

/**
 * Created by alena on 14.08.2016.
 */
public class LanguagesPresenter implements ILanguagesPresenter {

    private static String TAG = LanguagesPresenter.class.getSimpleName();

    private int fragmentType;
    private Activity activity;
    private ILanguagesView langsView;
    private ILangsInteractor langsInteractor;
    private LanguagesStorage langsStorage;
    private ArrayList<Language> languages;
    private Language srcLanguage;
    private Language targetLanguage;

    public LanguagesPresenter(Activity activity, int fragmentType){
        this.activity = activity;
        this.fragmentType = fragmentType;
    }

    @Override
    public void attachView(ILanguagesView view) {
        this.langsView = view;
    }

    @Override
    public void onCreate() {
        this.langsInteractor = new LangsInteractor();
        this.langsStorage = LanguagesStorage.getInstance(activity);
    }

    @Override
    public void onStart() {
        // try to get languages from storage
        srcLanguage = getCurrentLang(Utils.SRC_LANG, srcLanguage);
        targetLanguage = getCurrentLang(Utils.TARGET_LANG, targetLanguage);
        languages = langsStorage.getLanguages();

        if (languages != null) {
            langsView.showItems(languages);
            checkCurrentLangItem();
        } else {
            String currentLang = Utils.getCurrentLangCode();
            Log.d(TAG, "srcLanguage: " + currentLang);
            langsInteractor.getLangsRequest(currentLang, onLangsReceivedListener);
        }
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    private Language getCurrentLang(int langType, Language lang){
        if (lang != null) {
            return lang;
        } else {
            lang = langsStorage.getCurrentLang(langType);
            if (lang != null) {
                return lang;
            } else {
                lang = langsInteractor.getDefaultLang(languages, langType);
                langsInteractor.setCurrentLang(lang, langType, langsStorage,
                        ((MainActivity)activity).getOnLangChangedListener());
                return lang;
            }
        }
    }

    private void checkCurrentLangItem() {
        if (fragmentType == Utils.SRC_LANG && srcLanguage != null)
            langsView.setLangChecked(srcLanguage);

        if (fragmentType == Utils.TARGET_LANG && targetLanguage != null)
            langsView.setLangChecked(targetLanguage);
    }

    OnLangsReceivedListener onLangsReceivedListener = new OnLangsReceivedListener() {
        @Override
        public void onServerError(String msg) {
            Log.e(TAG, "Server error: " + msg);
            langsView.showToast(activity.getString(R.string.on_server_error));
        }
        @Override
        public void onSuccess(Languages response, Language srcLang, Language targetLang) {
            if (response.getLangs() != null) {
                Log.d(TAG, "onSuccess. languages size: " + response.getLangs().size());
                languages = response.getLangs();
                langsStorage.setLanguages(languages);

                srcLanguage = srcLang;
                targetLanguage = targetLang;
                langsInteractor.setCurrentLang(srcLang, Utils.SRC_LANG, langsStorage,
                        ((MainActivity)activity).getOnLangChangedListener());
                langsInteractor.setCurrentLang(targetLang, Utils.TARGET_LANG, langsStorage,
                        ((MainActivity)activity).getOnLangChangedListener());

                langsView.showItems(languages);
                checkCurrentLangItem();
            } else {
                Log.e(TAG, "Languages response is null!!!");
            }
        }
    };

    @Override
    public void onListItemClick(ListView listView, View view, int pos, long id) {
        Language lang = (Language)listView.getAdapter().getItem(pos);
        langsInteractor.setCurrentLang(lang, fragmentType, langsStorage,
                ((MainActivity)activity).getOnLangChangedListener());
    }
}
