package monakhova.alena.translator.mvp.view;

import java.util.List;

import monakhova.alena.translator.mvp.model.Translation;

/**
 * Created by alena on 13.08.2016.
 */
public interface IFavoritesView extends IView {

    void addItem(Translation translation);

    void removeItem(int id);

    void setFavoriteButtonStatus(int id, boolean isFavorite);

    void showSearchResults(List<Translation> list);

}
