package monakhova.alena.translator.mvp.interactor;

import monakhova.alena.translator.mvp.model.Languages;
import monakhova.alena.translator.mvp.model.Translation;

/**
 * Created by alena on 12.08.2016.
 */
public interface ITranslInteractor {

    interface OnTranslReceivedListener {
        void onServerError(String msg);
        void onSuccess(Translation translation);
    }

    void translationRequest(String srcText, String lang,
                            OnTranslReceivedListener onTranslReceivedListener);
}
