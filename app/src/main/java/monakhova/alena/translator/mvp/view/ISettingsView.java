package monakhova.alena.translator.mvp.view;

/**
 * Created by alena on 13.08.2016.
 */
public interface ISettingsView extends IView {

    void setDetectLang(boolean detectLang);

    void setEngine(int engineId);

}
