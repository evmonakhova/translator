package monakhova.alena.translator.storage;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

import monakhova.alena.translator.mvp.model.Language;
import monakhova.alena.translator.utils.Utils;

/**
 * Created by alena on 13.08.2016.
 */
public class LanguagesStorage {

    public static final String KEY_SRC = "SRC";
    public static final String KEY_TARGET = "TARGET";
    public static final String KEY_LANG_ID = "LANG_ID";
    public static final String KEY_LANG_NAME = "LANG_NAME";
    public static final String KEY_LANG_CODE = "LANG_CODE";
    public static final String KEY_LANG_AMOUNT = "LANG_AMOUNT";

    public static volatile LanguagesStorage instance;
    private final SharedPreferences preffs;

    public static LanguagesStorage getInstance(Context context){
        if (instance == null){
            synchronized (LanguagesStorage.class){
                if (instance == null){
                    instance = new LanguagesStorage(context);
                }
            }
        }
        return instance;
    }

    private LanguagesStorage(Context context){
        preffs = context.getSharedPreferences(LanguagesStorage.class.getName(),
                Context.MODE_PRIVATE);
    }

    public void setCurrentLang(Language lang, int langType){
        SharedPreferences.Editor editor = preffs.edit();

        if (langType == Utils.SRC_LANG) {
            editor.putInt(createKey(KEY_SRC, KEY_LANG_ID), lang.getId());
            editor.putString(createKey(KEY_SRC, KEY_LANG_CODE), lang.getCode());
            editor.putString(createKey(KEY_SRC, KEY_LANG_NAME), lang.getName());
        } else if (langType == Utils.TARGET_LANG){
            editor.putInt(createKey(KEY_TARGET, KEY_LANG_ID), lang.getId());
            editor.putString(createKey(KEY_TARGET, KEY_LANG_CODE), lang.getCode());
            editor.putString(createKey(KEY_TARGET, KEY_LANG_NAME), lang.getName());
        }

        editor.apply();
    }

    public Language getCurrentLang(int langType){
        int id = -1;
        String code = null, name = null;

        if (langType == Utils.SRC_LANG){
            id = preffs.getInt(createKey(KEY_SRC, KEY_LANG_ID), -1);
            code = preffs.getString(createKey(KEY_SRC, KEY_LANG_CODE), null);
            name = preffs.getString(createKey(KEY_SRC, KEY_LANG_NAME), null);
        } else if (langType == Utils.TARGET_LANG){
            id = preffs.getInt(createKey(KEY_TARGET, KEY_LANG_ID), -1);
            code = preffs.getString(createKey(KEY_TARGET, KEY_LANG_CODE), null);
            name = preffs.getString(createKey(KEY_TARGET, KEY_LANG_NAME), null);
        }

        if (id >= 0 && code != null && name != null)
            return new Language(id, code, name);

        return null;
    }

    public void setLanguages(ArrayList<Language> langs){
        SharedPreferences.Editor editor = preffs.edit();
        editor.putInt(KEY_LANG_AMOUNT, langs.size());
        for (Language lang : langs)
            setLanguage(lang);
        editor.apply();
    }

    private void setLanguage(Language lang){
        SharedPreferences.Editor editor = preffs.edit();
        editor.putString(createKey(KEY_LANG_CODE, lang.getId()), lang.getCode());
        editor.putString(createKey(KEY_LANG_NAME, lang.getId()), lang.getName());
        editor.apply();
    }

    public ArrayList<Language> getLanguages(){
        ArrayList<Language> langs = new ArrayList<>();
        int n = preffs.getInt(KEY_LANG_AMOUNT, 0);
        if (n != 0) {
            for (int i = 0; i < n; i++)
                langs.add(getLanguage(i));
            return langs;
        } else {
            return null;
        }
    }

    public String[] getLanguageNames(){
        int n = preffs.getInt(KEY_LANG_AMOUNT, 0);
        if (n != 0) {
            String[] names = new String[n];
            for (int i = 0; i < n; i++)
                names[i] = getLanguage(i).getName();
            return names;
        } else {
            return null;
        }
    }

    public String getLangCode(int id){
        return preffs.getString(createKey(KEY_LANG_CODE, id), "");
    }

    public String getLangName(int id){
        return preffs.getString(createKey(KEY_LANG_NAME, id), "");
    }

    public Language getLanguage(int id){
        return new Language(id, getLangCode(id), getLangName(id));
    }


    private static String createKey(String prefix, int id){
        return prefix + "_" + id;
    }

    private static String createKey(String prefix1, String prefix2){
        return prefix1 + "_" + prefix2;
    }
}
