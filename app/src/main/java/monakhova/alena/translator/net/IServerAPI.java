package monakhova.alena.translator.net;

import monakhova.alena.translator.mvp.model.Languages;
import monakhova.alena.translator.mvp.model.Translation;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface IServerAPI {

    @GET("getLangs")
    Call<Languages> getLangs(@Query("key") String key, @Query("ui") String ui);

//    @POST("translate")
//    Call<Translation> translate(@Query("key") String key, @Query("lang") String lang, @Body String text);

    @GET("translate")
    Call<Translation> translate(@Query("key") String key, @Query("text") String text, @Query("lang") String lang);

}
