package monakhova.alena.translator.net;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by alena on 14.08.2016.
 */
public class ApiClient {

    public static final String BASE_URL = "https://translate.yandex.net/api/v1.5/tr.json/";
    public static final String KEY = "trnsl.1.1.20160807T213834Z.59cc1d49af077d10.c9ec81da4bb032c8fd424056aaf4cdf577774d2b";

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
